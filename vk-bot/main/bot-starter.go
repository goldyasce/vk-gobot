package main

import (
	s "strings"
	"time"
	govkbot "vk-bot"
	"vk-bot/event"
	"vk-bot/vk"
)

const (
	groupID = "193586002"
	token   = "386f42d83e4d7312798b0b717fb96a2d0fafd0a2c9a9bae8b0ee3b00bfc564e9f52de560b0fda07743432"
	version = "5.103"
	prefix  = "MyAwesomeBot"
	price = "Стрижка наголо - 100p, 10мин.\n\tСтрижка под одну насадку - 150p, 15мин.\n\tСтрижка мужская -  300p, 20мин.\n\tМытье волос - 50p, 10мин.\n\tСтрижка женская короткие волосы - 200p, 15мин.\n\tСтрижка женская длинные волосы - 300p, 20мин.\n\tСтрижка челки - 100p, 10мин.\n\tОкрашивание волос (средство фирмы Estel) - 500p, 60мин.\n\tПлетение - 300р, 30мин."
	commandList = "\n\tРаботаете\n\tГрафикРаботы\n\tРаботающиеМастера\n\tАдрес\n\tНомер\n\tМаникюр\n\tУслуги"
	schedule = "\tПонедельник: с 9:00 до 19:00.\n\tВторник: с 9:00 до 19:00.\n\tСреда: с 9:00 до 19:00.\n\tЧетверг: с 9:00 до 19:00.\n\tПятница: с 9:00 до 19:00.\n\tСуббота: с 10:00 до 18:00.\n\tВоскресенье: с 10:00 до 18:00.\n"
	address = "https://www.google.com/maps/place/%D1%83%D0%BB.+%D0%A1%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%BE%D0%B2%D0%B0,+56,+%D0%A0%D1%8F%D0%B7%D0%B0%D0%BD%D1%8C,+%D0%A0%D1%8F%D0%B7%D0%B0%D0%BD%D1%81%D0%BA%D0%B0%D1%8F+%D0%BE%D0%B1%D0%BB.,+390026/@54.6149493,39.7132997,17z/data=!3m1!4b1!4m5!3m4!1s0x4149e215fa1011d7:0x18f4b4a90a511ff5!8m2!3d54.6149493!4d39.7154884"
)

var bot *govkbot.Bot

func main() {
	bot = govkbot.CreateBot(groupID, token, version)

	// Handling `test` command
	bot.OnCommand("test", func(args []string, command *event.Command) bool {
		bot.SendMessage("Testing", command.PrivateMessage.Message.PeerID, vk.H{})
		return true
	})

	bot.OnCommand("help", func(args []string, command *event.Command) bool {
		bot.SendMessage("Справка", command.PrivateMessage.Message.PeerID, vk.H{})
		return true
	})

	bot.OnCommand("Услуги", func(args []string, command *event.Command) bool {
		bot.SendMessage("Наши услуги:\n" + price, command.PrivateMessage.Message.PeerID, vk.H{})
		return true
	})

	bot.OnCommand("Маникюр", func(args []string, command *event.Command) bool {
		bot.SendMessage("У нас есть услуги маникюра.\n\nСнятие покрытия 100₽\nМаникюр 300₽\nПокрытие гель-лаком 600₽\n\nЗапись производится по телефону: +7(4912)76-26-62.", command.PrivateMessage.Message.PeerID, vk.H{})
		return true
	})

	bot.OnCommand("Номер", func(args []string, command *event.Command) bool {
		bot.SendMessage("Наш телефон: +7(4912)76-26-62.", command.PrivateMessage.Message.PeerID, vk.H{})
		return true
	})

	bot.OnCommand("Работаете", func(args []string, command *event.Command) bool {
		dayTime := time.Now()
		switch dayTime.Weekday() {
		case time.Saturday, time.Sunday:
			if dayTime.Hour() < 18 {
				bot.SendMessage("Мы сегодня работаем до 18:00.\nВы можете записаться к нам по телефону: +7(4912)76-26-62.", command.PrivateMessage.Message.PeerID, vk.H{})
			} else {
				bot.SendMessage("К сожалению, сегодня мы уже закрыты.\nВы можете узнать наш график работы командой ГрафикРаботы.", command.PrivateMessage.Message.PeerID, vk.H{})
			}
		default:
			if dayTime.Hour() < 19 {
				bot.SendMessage("Мы сегодня работаем до 19:00.\nВы можете записаться к нам по телефону: +7(4912)76-26-62.", command.PrivateMessage.Message.PeerID, vk.H{})
			} else {
				bot.SendMessage("К сожалению, сегодня мы уже закрыты.\nВы можете узнать наш график работы командой ГрафикРаботы.", command.PrivateMessage.Message.PeerID, vk.H{})
			}
		}
		return true
	})

	bot.OnCommand("РаботающиеМастера", func(args []string, command *event.Command) bool {
		dayTime := time.Now()
		switch dayTime.Weekday() {
		case time.Monday, time.Tuesday, time.Friday, time.Saturday:
			bot.SendMessage("Сегодня работают Алла и Татьяна.", command.PrivateMessage.Message.PeerID, vk.H{})
		default:
			bot.SendMessage("Сегодня работают Виктория и Людмила.", command.PrivateMessage.Message.PeerID, vk.H{})
		}
		return true
	})

	bot.OnCommand("ГрафикРаботы", func(args []string, command *event.Command) bool {
		bot.SendMessage("График работы:\n" + schedule, command.PrivateMessage.Message.PeerID, vk.H{})
		return true
	})

	bot.OnCommand("Адрес", func(args []string, command *event.Command) bool {
		bot.SendMessage("Рязань, улица Стройкова, дом 56, вход с торца.\nМожете поглядеть на Google Maps: " + address, command.PrivateMessage.Message.PeerID, vk.H{})
		return true
	})

	// Handling of all messages except commands
	bot.On(event.MessageNewEvent, func(e event.Event) bool {
		// This is necessary to get rid of a heap of methods like `OnNewWallReply` or `OnEditWallReply`
		ev := e.(*event.MessageNew)
		dayTime := time.Now()

		message := ev.PrivateMessage.Message.Text

		if s.Contains(s.ToLower(message), "работ") {
			switch dayTime.Weekday() {
			case time.Saturday, time.Sunday:
				if dayTime.Hour() < 18 {
					bot.SendMessage("Да, работаем, будем Вас ждать", ev.PrivateMessage.Message.PeerID, vk.H{})
				} else {
					bot.SendMessage("К сожалению, нет", ev.PrivateMessage.Message.PeerID, vk.H{})
				}
			default:
				if dayTime.Hour() < 19 {
					bot.SendMessage("Да, работаем, будем Вас ждать", ev.PrivateMessage.Message.PeerID, vk.H{})
				} else {
					bot.SendMessage("К сожалению, нет", ev.PrivateMessage.Message.PeerID, vk.H{})
				}
			}
		} else if s.Contains(s.ToLower(message), "скольк") && s.Contains(s.ToLower(message), "до") {
			switch dayTime.Weekday() {
			case time.Saturday, time.Sunday:
				bot.SendMessage("До 18:00", ev.PrivateMessage.Message.PeerID, vk.H{})
			default:
				bot.SendMessage("До 19:00", ev.PrivateMessage.Message.PeerID, vk.H{})
			}
		} else if s.Contains(s.ToLower(message), "кто") || s.Contains(s.ToLower(message), "маст"){
			switch dayTime.Weekday() {
			case time.Monday, time.Tuesday, time.Friday, time.Saturday:
				bot.SendMessage("Сегодня работают Алла и Татьяна.", ev.PrivateMessage.Message.PeerID, vk.H{})
			default:
				bot.SendMessage("Сегодня работают Виктория и Людмила.", ev.PrivateMessage.Message.PeerID, vk.H{})
			}
		} else if s.Contains(s.ToLower(message), "адр") || s.Contains(s.ToLower(message), "где") || s.Contains(s.ToLower(message), "найти") {
			bot.SendMessage("Рязань, улица Стройкова, дом 56, вход с торца.\nМожете поглядеть на Google Maps: " + address, ev.PrivateMessage.Message.PeerID, vk.H{})
		} else if s.Contains(s.ToLower(message), "теле") || s.Contains(s.ToLower(message), "номер") || s.Contains(s.ToLower(message), "связ") {
			bot.SendMessage("Наш телефон: +7(4912)76-26-62.", ev.PrivateMessage.Message.PeerID, vk.H{})
		} else if s.Contains(s.ToLower(message), "прайс") || s.Contains(s.ToLower(message), "цен") || s.Contains(s.ToLower(message), "усл") {
			bot.SendMessage("Наши услуги:\n" + price, ev.PrivateMessage.Message.PeerID, vk.H{})
		} else if s.Contains(s.ToLower(message), "стоим") || s.Contains(s.ToLower(message), "длит") || s.Contains(s.ToLower(message), "врем") || s.Contains(s.ToLower(message), "стоит") {
			bot.SendMessage("Наши услуги:\n" + price, ev.PrivateMessage.Message.PeerID, vk.H{})
		} else {
			bot.SendMessage("Добрый день!\nВас приветствует дружеская форма жизни!\nВы можете воспользоваться списком конманд, которые я умею делать или задать интересующий Вас вопрос!\n Список команд:" + commandList, ev.PrivateMessage.Message.PeerID, vk.H{})
		}

		bot.GetLogger().Info(ev.GetName())
		return true
	})

	// Starting the bot and waiting the events
	bot.Polling()
}