package event

const (
	CommandEvent = "message_command"
	// messages
	// input message
	MessageNewEvent = "message_new"
	// output message
	MessageReplyEvent = "message_reply"
	// edit message
	MessageEditEvent = "message_edit"
	// subscribe on messages
	MessageAllowEvent = "message_allow"
	// unsubscribe from messages
	MessageDenyEvent = "message_deny"
)