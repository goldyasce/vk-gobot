module github.com/AlexBrin/goVkBot

go 1.12

require (
	github.com/fatih/color v1.9.0
	github.com/mitchellh/mapstructure v1.3.0
)
